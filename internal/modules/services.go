package modules

import (
	"gitlab.com/lear4212225/go-rpc-main/internal/infrastructure/component"
	aservice "gitlab.com/lear4212225/go-rpc-main/internal/modules/auth/service"
	uservice "gitlab.com/lear4212225/go-rpc-main/internal/modules/user/service"
	"gitlab.com/lear4212225/go-rpc-main/internal/storages"
)

type Services struct {
	User          uservice.Userer
	Auth          aservice.Auther
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
