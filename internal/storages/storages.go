package storages

import (
	"gitlab.com/lear4212225/go-rpc-main/internal/db/adapter"
	"gitlab.com/lear4212225/go-rpc-main/internal/infrastructure/cache"
	vstorage "gitlab.com/lear4212225/go-rpc-main/internal/modules/auth/storage"
	ustorage "gitlab.com/lear4212225/go-rpc-main/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
